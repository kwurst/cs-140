*Version 2020-Summer-1.4, Revised 28 May 2020*

### *CS-140 AL &mdash; Summer I 2020*

# CS-140 Introduction to Programming

## Credit and Contact Hours
4 credits

Lecture and Lab &mdash; 6 hours/week synchronous online and 2 hours/week asynchronous online

## Catalog Course Description
> *Introduction to fundamental structures and concepts of Computer Science including object-oriented programming.*

## Instructor
Dr. Karl R. Wurst<br>
See <a href="http://cs.worcester.edu/kwurst/" target="_blank">http://cs.worcester.edu/kwurst/</a> for contact information and schedule.

## Meeting Times and Locations
* Synchronous "Lecture" and Lab
	* TR 1:00pm-4:00pm EDT (UTC-04:00)
* Office hours
	* TR 12:30-1:00pm EDT (UTC-04:00) (before class)
	* TR 4:00-4:40pm EDT (UTC-04:00) (after class)
	* By appointment (email me)

## It's in the Syllabus
<img src="http://www.phdcomics.com/comics/archive/phd051013s.gif"><br>
<a href="http://www.phdcomics.com/comics.php?f=1583">http://www.phdcomics.com/comics.php?f=1583</a>

Of course, not *everything* is in the syllabus, but many things are.

If you have a question, please check the syllabus first, and then ask if you can't find the answer.

## Textbook
*Java Early Objects*<br>
Roman Lysecky and Adrian Lizarraga<br>
zyBooks<br>
[Textbook Website](http://www.zybooks.com/catalog/java-early-objects/)

#### Purchasing information
1. Sign in or create an account at [learn.zybooks.com](https://learn.zybooks.com)
2. Enter zyBook code: `WORCESTERCS140WurstSummer2020`
3. Subscribe

#### Additional information
* A subscription is $58 and will last until Jul 17, 2020.
* [What if I am retaking a class, can I get a discount?](https://zybooks.zendesk.com/hc/en-us/articles/360007439634-If-I-am-retaking-a-class-can-I-get-a-discount-)

## Required Materials
In addition to the textbook, to successfully complete this course you will need:

1. **A computer**: You will need a computer that you can use at home.
	* ***Your computer must be availble for your use during every synchronous class session &mdash; TR 1:00pm-4:00pm EDT (UTC-04:00)
	&mdash; as well as outside of synchronous class times for assignments.***
	* The brand and operating system (Windows, Mac OS X, Linux) is unimportant. **NOTE: Chromebooks are not suitable as you will not be
	able to install the software.**<br>
	* The software we will be using runs on all major operating systems and can be downloaded for free.  The software you will need
	installed:
		1. **BlueJ** &mdash; This is the Java programming environment we will be using. We will download and install this software during
		the first laboratory session.
		2.	**Git** &mdash; We will use this software to share code during lab, and submit code to the instructor. We will download and
		install this software during the first laboratory session.
2.	**Internet Access**: You will need internet access for access to:
	1. **Zoom** &mdash; Synchronous class meetings and office hours will take place on Zoom.
	2. **ZyBooks** &mdash; Our textbook and Java code-writing practice.
	3. **Blackboard** &mdash; All course materials and announcements will be made available through the course site on Blackboard.
	Quizzes and Exams will be given on Blackboard. Students will be required to use Blackboard as the course tool.
	4. **WSU Gmail** &mdash; You must check your WSU Gmail account on a regular basis (no less than once per day). All communications to
	the class, such as corrections to problem sets or changes in due dates, will be sent to your WSU Gmail account.
	5. **Piazza** &mdash; This is an online service that lets you ask (and answer other students’) questions about the course
	assignments, materials, and topics.
	6. **GitLab** &mdash; This is where we will host and submit our code.

## Where Does This Course Lead?
* CS-242 Data Structures
* CS-254 Computer Organization and Architecture
* CS-286 Database Design and Applications
* Your professional career.

## Course Workload Expectations
***This is a four-credit course with laboratory in a seven-week summer session. You should expect to spend, on average, 22 hours per
week on this class.***

In a normal (Fall or Spring) 14-week semester, the face-to-face version of this course would have an expectation of 11 hours per week
of work (See *Definition of the Credit Hour*):
* 3 hours of lecture/discussion
* 2 hours of lab
* 6 hours of work outside the classroom reading the textbook, studying, doing assignments, working on projects, and practicing in
order to master the concepts and techniques covered in the course.

In a seven-week summer session, we cover two normal semester weeks of work in each summer session week, for a total of 22 hours per
week:
* 6 hours of synchronous classwork (lecture/discussion and lab)
* 4 hours of preparation and "finishing up" for the synchronous classwork
* 12 hours of work outside class

## Definition of the Credit Hour
> Federal regulation defines a credit hour as an amount of work represented in intended learning outcomes and verified by evidence of
student achievement that is an institutional established equivalence that reasonably approximates not less than –

> 1.	One hour of classroom or direct faculty instruction and a minimum of two hours of out of class student work each week for
approximately fifteen weeks for one semester or trimester hour of credit, or ten to twelve weeks for one quarter hour of credit, or
the equivalent amount of work over a different amount of time; or
> 2.	At least an equivalent amount of work as required in paragraph (1) of this definition for other academic activities as
established by the institution including laboratory work, internships, practica, studio work, and other academic work leading to the
award of credit hours.

> &mdash; New England Commission of Higher Education,
<a href="https://www.neche.org/wp-content/uploads/2018/12/Pp111_Policy_On_Credits-And-Degrees.pdf" target="_blank">
Policy on Credits and Degrees</a>

## Prerequisites
Since this course has a prerequisite or corequisite of Basics of Computer Science (CS-101), I assume that you have a basic
understanding of:
* how computers represent and manipulate data
* binary numbers and their operations
* boolean operations (AND, OR, NOT)
* the concept of an algorithm* the pseudocode representation of an algorithm
* the algorithmic principles of assignment, conditionals, and repetition

***If you do not have this background, you should not take this course.***

## Attendance
You are expected to attend every class. Past experience has shown that students who do not attend class do not do as well on exams and
projects.

## Course-Level Student Learning Outcomes
After successful completion of this course, students will be able to:

* Understand the basic syntax and semantics of the Java programming language. (Emphasis)
* Model an abstraction as a Java class. (Emphasis)* Distinguish between the concepts of class and object. (Mastery)
* Manipulate integer and floating point values. (Mastery)
* Code, compile, test and debug simple object-oriented programs given a program design/specification. (Emphasis)
* Write simple methods to access and manipulate data. (Emphasis)
* Accept user input from the keyboard, as command-line arguments and read data from a text file. (Emphasis)
* Generate output to the screen and to a text file. (Emphasis)
* Trace, code, test, and debug simple recursive functions and procedures. (Introduction)
* Manipulate and compare strings. (Emphasis)* Store and access data in an array. (Emphasis)
* Perform simple exception handling. (Introduction)
* Write code representing conditional and repetition control structures. (Mastery)
* Understand the difference between inheritance and an interface. (Introduction)
* Understand the concept of inheritance and write simple inheritance hierarchies. (Introduction)
* Trace code to determine the behavior of the code (Emphasis)
* Draw a diagram representing memory and the object/reference relationships (Emphasis)
* Write simple JUnit tests (Introduction)
* Use a version control system to commit and revert changes on a single-user repository (Introduction)
* Document code for other programmers, users of classes (javadoc), users of programs (Emphasis)

## LASC Student Learning Outcomes
This course does not fulfill any LASC Content Area requirements, but contributes to the following Overarching Outcomes of LASC:

* Demonstrate effective oral and written communication.
* Employ quantitative and qualitative reasoning.
* Apply skills in critical thinking.
* Apply skills in information literacy.
* Understand the roles of science and technology in our modern world.
* Understand how scholars in various disciplines approach problems and construct knowledge.

## Program-Level Student Learning Outcomes
This course addresses the following outcomes of the Computer Science Major:
Students will be able to:

1.	Analyze a problem, develop/design multiple solutions and evaluate and document the solutions based on the requirements.
(Introductory Level)
2.	Communicate effectively both in written and oral form. (Introductory Level)
4.	Demonstrate an understanding of and appreciation for the importance of negotiation, effective work habits, leadership, and good
communication with teammates and stakeholders. (Introductory Level)
5.	Learn new models, techniques, and technologies as they emerge and appreciate the necessity of such continuing professional
development. (Introductory Level)

## Course Topics
The course outline will be covered on a best-effort basis, subject as always to time limitations as the course progresses.  

* Programming Languages vs. Natural Languages
	* Translation, Interpretation, Compilation
	* Syntax and Semantics
* Algorithms
* Testing and Debugging
	* Junit
* Variables, Assignment, Operators
* Objects and Classes
	* Instance and Class Methods
* Control Flow
	* Repetition
	* Selection
	* Tracing code
* String operations and comparisons
* Input/Output
	* Command line arguments
	* File I/O
* Arrays and ArrayList
* Exceptions
* Inheritance
* Interfaces
* Simple version control
	* Git
* Documentation
	* Javadoc

## Course Philosophy
Even though this course will be using the Java programming language and its associated libraries, much of what you will be learning is
language independent. Therefore, we will place an emphasis on concepts and techniques, in addition to the particulars of the Java
programming language.
You are encouraged to help each other out, in and out of the classroom, as long as you do your own work. (See Academic Conduct below.)

## Instructional Methods
This class will not be a traditional “lecture” class, and will incorporate some teaching methods that may be unfamiliar to you.

### POGIL
Rather than having the instructor lecture about the course content, you and your classmates will "discover" the content for yourselves
through small-group work.

The group work will be a very structured style called Process Oriented Guided Inquiry Learning (POGIL). Through investigation of
models of the concepts and answering questions that guide the team toward understanding of the models, your team will both learn the
content and team process skills. In your POGIL groups each group member will have a specific role to play during the activity, and
roles will be rotated so that everyone will get to experience a variety of process skills.

For more information on POGIL, see [https://pogil.org/about-pogil/what-is-pogil](https://pogil.org/about-pogil/what-is-pogil).

### Pair Programming
During labs, you will work in a pair with one of your classmates. One of you will type while the other makes suggestions, watches for
errors, reads the assignment, and thinks ahead. You will switch roles frequently during the lab session.

You will be randomly assigned a new partner each lab. This will allow you to get to know the other members of the class, work with
partners of different abilities and programming styles, and develop relationships that may extend beyond to classroom.

## Grading Policies
I want everyone receiving a passing grade in this course to be, at least, minimally competent in the course learning outcomes and for
that to be reflected in your course grade. Traditional grading schemes do a poor job of indicating competency.

As an example, imagine a course with two major learning outcomes: *X* and *Y*. It is widely considered that a course grade of C
indicates that a student is minimally competent in achieving the course outcomes. However, if the student were
to receive a grade of 100 for outcome *X*, and a grade of 40 for outcome *Y*, the student would still have a 70 (C-) average for the
course. Yet the student is clearly not competent in outcome *Y*.

Therefore the grading in this course will be handled in a different manner:

* All assignments will be graded on a ***Meets Specification*** / ***Does Not Yet Meet Specification*** basis, based on whether the
student work meets the instructor-supplied specification.
* A minimum collection of assignments, indicating competency in the course learning outcomes, must be completed in a ***Meets
Specification*** manner to earn a passing course grade (D).
* Higher passing grades (A, B, C) can be earned by completing more assignments and/or assignments that show higher-level thinking and
learning skills.

### Assignment Grading

* All assignments in this course will be graded exclusively on a ***Meets Specification*** / ***Does Not Yet Meet Specification***
basis.
* **For each assignment, you will be given a detailed specification explaining what is required for the work to be marked *Meets
Specification*.**
* Failing to meet ***any part*** of the specification will result in the work being marked **Does Not Yet Meet Specification**.
* There will be no partial credit given.
* If you are unclear on what the specification requires, it is your responsibility to ask me for clarification.
* It will be possible to revise and resubmit a limited number of assignments with **Does Not Yet Meet Specification** grades (see
*Revision and Resubmission of Work* below).

### Course Grade Determination

Your grade for the course will be determined by which assignments and/or how many assignments you complete in an Meets Specification
manner.

#### Base Grade

Assignment | Earn Base Grade<br>A | Earn Base Grade<br>B | Earn Base Grade<br>C | Earn Base Grade<br>D
--- | :-: | :-: | :-: | :-:
Attendance and Participation (out of 13) | 12 | 12 | 11 | 10
Readings Quizzes Average | 90% | 80% | 70%  | 60%
zyBooks Practice<br>&nbsp;&nbsp;&mdash; Participation Activities<br>&nbsp;&nbsp;&mdash; Challenge Activities | <br>95% <br> 85% | <br>85% <br> 75% | <br>75% <br> 65% | <br>65% <br> 55%
Laboratory Assignments (out of 11) | 10 | 10 | 9 | 8
Programming Projects (out 6)<br>&nbsp;&nbsp;&mdash; Submitted, with appropriate classes and methods, compile and run<br>&nbsp;&nbsp;&mdash; Meet Specification  | <br>6<br>6 | <br>6<br>5 | <br>6<br>4  | <br>5<br>3
Discussion Logs<br>&nbsp;&nbsp;&mdash; Lab Assignments (out of 11)<br>&nbsp;&nbsp;&mdash; Programming Projects (out of 6) | <br>10<br>5 | <br>7<br>4 | <br>4<br>3 | <br>1<br>2
Exam Grade Average (3 exams) | > 50% | > 50% | > 50% | &le; 50%

* **Failing to meet the all the requirements for a particular letter grade will result in not earning that grade.** For example, even
if you complete all other requirements for a B grade, but fail to submit 5  Programming Assignment Disussion Logs that meet specification, you will earn a C grade.
* **Failing to meet the all the requirements for earning a D grade will result in a failing grade for the course.**

#### Plus or Minus Grade Modifiers

* You will have a ***minus*** modifier applied to your base grade if the average of your exam grades is lower than 60%.
* You will have a ***plus*** modifier applied to your base grade if the average of your exam grades is 85% or higher.
* Each unused token remaining at the end of the semester can be used to increase the exam average by 2 percentage points.

*Notes:*

* WSU has no A+ grade.
* I reserve the right to revise *downward* the required number of assignments needed for each base grade due to changes in number of
assignments assigned or unexpected difficulties with assignments.
## Class and Lab Attendance and Participation
Since there will be no lecture during class, it is particularly important that you attend class sessions and participate in developing
the material and concepts with your group. Simply being present is not enough to really learn the material - you need to play your
part.

During class time you are expected to participate in your group, to play your assigned role, and to contribute to your groups's notes.

During lab time, you will be working with your lab partner to complete the lab assignment.

**You will be using your computer within your group activities and labs. You should only be using your computer for the classwork. You
should not be checking sites that are unrelated to the course, or messaging friends. If I see that you are doing other things on your
computer and not participating in your group, I will warn you. If I have to warn you a second time during that class, you will lose
your participation points for that class.**

## Quizzes on Readings
You will take quizzes on the readings so that I may assess where students are having conceptual difficulties with the material. In
addition to giving an answer to the quiz questions, you will required to explain your reasoning for the answers you chose.

***All quizzes can be attempted an unlimited number of times before the due date/time. You are allowed to ask me questions by email or
in office hours when working on quizzes.***

## zyBooks Practice
The zyBooks textbook contains both Participation activities, where credit is given simply for trying the activity, and Challenge
activities which require a correct answer.

***All activities can be attempted an unlimited number of times before the due date/time. You are allowed to ask me questions by email
or in office hours when working on zyBooks activities.***

## Laboratory Assignments
The laboratory assignments provide you with the chance to explore the Java language and development tools in an incremental, guided
way. This will help you develop confidence and mastery before you are asked to complete programming projects on your own.

## Programming Projects
The programming projects will give you a chance to apply the material to larger tasks. These projects will require you to understand a
design, implement that design in code, test, and debug it. Your code will have to follow formatting and documentation standards.

## Exams
Exams will be taken online, outside of class time. The exams will be timed, but you will be able each at any time within specified time period.
* Exam 1 will be posted after class on Thursday, 4 June 2020, and will be due before class on Tuesday, 9 June 2020
* Exam 2 will be posted after class on Thursday, 18 June 2020, and will be due before class on Tuesday, 23 June 2020
* Exam 3 will be posted after class on Tuesday, 30 June 2020, and will be due by the end of the day on Friday, 3 July 2020

## Deliverables
All work must be submitted electronically. The submission method, due date and time will be given on the assignment. The submission date and time will be determined by the timestamp in Blackboard, zyBooks, or GitLab.

**Please do not submit assignments to me via email.** It is difficult for me to keep track of them and I often fail to remember that
they are in my mailbox when it comes time to grade the assignment.

It is strongly recommended that you keep copies of your projects. Students are responsible for reproducing any lost work including
unreadable files.

Graded assignments (with comments and solutions) will be returned to you electronically. *Please make sure that you review the
comments and solutions provided so you can improve your future work.*

## Late Submissions
Late work will not be accepted. (See *Tokens* below.)

## Revision and Resubmission of Work

You may resubmit each lab assignment and programming assignment a second time if your first attempt "Does not yet meet specification".
If you need to resubmit an assignment a third time, you will have to spend a token.

## Tokens
Each student will be able to earn up to 5 tokens over the course of the semester. These tokens will be earned by completing simple set-
up and housekeeping tasks for the course.

Each token can be used to:

* replace a single missed class session (up to a maximum of 2 missed class sessions)
* turn in an assignment late by 24 hours
* revise and resubmit an assignment that was judged "Does Not Yet Meet Specification" beyond the second attempt. Any work to be
revised and resubmitted must have been submitted by the original due date.

### Token Accounting
* Unused tokens will be kept track of in the Blackboard *My Grades* area.
* Tokens will not be automatically applied. You must explicitly tell me **by email** when you want to use a token, and for which
assignment.

## Getting Help
If you are struggling with the material or a project please see me as soon as possible. Often a few minutes of individual attention is
all that is needed to get you back on track.

By all means, try to work out the material on your own, but ask for help when you cannot do that in a reasonable amount of time. The
longer you wait to ask for help, the harder it will be to catch up.

**Asking for help or coming to see me during office hours is not bothering or annoying me. I am here to help you understand the
material and be successful in the course.**

## Contacting Me
You may contact me by email (<a href="mailto:Karl.Wurst@worcester.edu">Karl.Wurst@worcester.edu</a>), telephone (+1-508-929-8728), or
see me during office hours on Zoom. My office hours are listed on the schedule on my web page
(<a href="http://cs.worcester.edu/kwurst/" target="_blank">http://cs.worcester.edu/kwurst/</a>) or you may make an appointment for a
mutually convenient time.

**If you email me, please include "[CS-140]" in the subject line, so that my email program can correctly file your email and ensure
that your message does not get buried in my general mailbox.**

**If you email me from an account other than your Worcester State email, please be sure that your name appears somewhere in the email,
so that I know who I am communicating with.**

<img src="http://www.phdcomics.com/comics/archive/phd042215s.gif"><br>
<a href="http://www.phdcomics.com/comics.php?f=1795">http://www.phdcomics.com/comics.php?f=1795</a>

You may expect that I will get back to you within 24 hours of your email or phone call (with the exception of weekends and holidays),
although you will likely hear from me much sooner.

## Code of Conduct/Classroom Civility
All students are expected to adhere to the policies as outlined in the University's Student Code of Conduct
(<a href="http://www.worcester.edu/CodeofConduct/" target="_blank">http://www.worcester.edu/CodeofConduct/</a>).

## Student Responsibilities
* Contribute to a class atmosphere that is conducive to learning for everyone by asking/answering questions, participating in class
discussions. Don't just lurk!
* Seek help when necessary.
* Start assignments as soon as they are posted. Do not wait until the due date to seek help/to do the assignments.
* Make use of the academic success center (see below).
* Expect to spend at least 22 hours of work per week on class work.
* Each student is responsible for the contents of the textbook readings, handouts, and homework assignments.

## Accessibility Statement
Worcester State University values the diversity of all of our students, faculty and staff.  We recognize the importance of each
student’s contribution to our campus community.  WSU is committed to providing equal access and support to all qualified students
through the provision of reasonable accommodations so that each student may fully participate in programs and services at WSU.  If you
have a disability that requires reasonable accommodations, please contact Student Accessibility Services at SAS@worcester.edu or 508-
929-8733.  Please be aware that accommodations cannot be enacted retroactively, making timeliness a critical aspect for their
provision.

## Tutoring Services/Academic Success Center
Tutoring Services are offered through the Academic Success Center (ASC).  The ASC is located on the first floor of the Administration
building, A-130.  Tutoring services are provided to students FREE of charge.  Students seeking academic assistance should visit the
center as soon as possible or contact the Tutoring Coordinator at 508-929-8139

## The Math Center
The Math Center provides free assistance to students in Mathematics.  It is located on the first floor of the Sullivan Academic
Building, S143.

## The Writing Center
The writing center provides free assistance to students in the areas of research and writing.  It is located on the third floor of the
Sullivan Academic Building, S306.  To schedule an appointment, please call 508-929-8112 or email the Center at
writingcenter@worcester.edu.  To find out more information about the Writing Center including the Center's hours and the Center's
Online Writing Lab, visit their website at <a href="http://www2.worcester.edu/WritingCenter/"
target="_blank">http://www2.worcester.edu/WritingCenter/</a>.

## Worcester State Library
Worcester State Library has access to many articles through online databases including J-STOR. In addition many articles and book
chapters are available to students through Inter-Library Loan (ILL).  With a little planning, ILL expands your ability to get credible
information sources about topics you pursue in your course work.  Finally WSU students are free to use many of the library resources
within the consortium. Given all of these resources it is extremely unlikely that you should have to pay for access to individual
articles. Please work with the reference librarians to find the appropriate way to access materials you need.  You have already paid
for these resources through your fees—please make use of them.

## Academic Conduct
Each student is responsible for the contents of the readings, discussions, class materials, textbook and handouts. All work must be
done independently unless assigned as a group project. You may discuss assignments and materials with other students, but you should
never share answers or files. ***Everything that you turn in must be your own original work, unless specified otherwise in the
assignment.***

Students may help each other understand the programming language and the development environment but students may not discuss actual
solutions, design or implementation, to their programming assignments before they are submitted or share code or help each other debug
their programming assignments. The assignments are the primary means used to teach the techniques and principles of computer
programming; only by completing the programs individually will students receive the full benefit of the assignments. If you are
looking at each other’s code before you submit your own, you are in violation of this policy.

I will be using software that compares submitted programs to identify cheating. This program is not fooled by changing variable names
or rearranging code.

Students may not use solutions to assignments from any textbooks other than the text assigned for the course, or from any person other
than the instructor, or from any internet site, or from any other source not specifically allowed by the instructor. If a student
copies a solution from an unauthorized source and submits it as a solution to an assignment, the student will receive a 0 for that
assignment.

**Any inappropriate sharing of work or use of another's work without attribution will result in a grade of zero on that assignment for
all parties involved. If you do so a second time, you will receive an "E" for the course.**

Academic integrity is an essential component of a Worcester State education. Education is both the acquisition of knowledge and the
development of skills that lead to further intellectual development. Faculty are expected to follow strict principles of intellectual
honesty in their own scholarship; students are held to the same standard. Only by doing their own work can students gain the
knowledge, skills, confidence and self-worth that come from earned success; only by learning how to gather information, to integrate
it and to communicate it effectively, to identify an idea and follow it to its logical conclusion can they develop the habits of mind
characteristic of educated citizens. Taking shortcuts to higher or easier grades results in a Worcester State experience that is
intellectually bankrupt.

Academic integrity is important to the integrity of the Worcester State community as a whole. If Worcester State awards degrees to
students who have not truly earned them, a reputation for dishonesty and incompetence will follow all of our graduates. Violators
cheat their classmates out of deserved rewards and recognition. Academic dishonesty debases the institution and demeans the degree
from that institution.  

It is in the interest of students, faculty, and administrators to recognize the importance of academic integrity and to ensure that
academic standards at Worcester State remain strong. Only by maintaining high standards of academic honesty can we protect the value
of the educational process and the credibility of the institution and its graduates in the larger community.

**You should familiarize yourself with Worcester State College's [Academic Honesty policy](https://catalog.worcester.edu/undergraduate/academic-policies-procedures/academic-honesty/). The policy outlines what constitutes
academic dishonesty, what sanctions may be imposed and the procedure for appealing a decision.**

**If you have a serious problem that prevents you from finishing an assignment on time, contact me and we'll come up with a solution.**

## Student Work Retention Policy
It is my policy to securely dispose of student work one calendar year after grades have been submitted for a course.

## Schedule
Our first class will be on Tuesday, 19 May 2020.<br>Our last class will be on Thursday, 2 July 2020.<br>

*The following course schedule is subject to change.*

&nbsp; | Tuesday | Thursday | Friday through Monday
:-: | :-: | :-: | :-: |
Week 1 | 19 May 2020<br>Activities 0 & 1 | 21 May 2020<br>Activities 2, 3, & 4, Lab 1A
Week 2 | 26 May 2020<br>Activity 5, Lab 1B | 28 May 2020<br>Activity 6, Lab 2
Week 3 | 2 June 2020<br>Activity 7, Lab 3 |4 June 2020<br>Activity 8, Lab 4<br>Exam 1 &rarr; | &rarr; Exam 1 &rarr;
Week 4 | 9 June 2020<br> &rarr; Exam 1<br>Activity 9, Lab 5 | 11 June 2020<br>Activity 10, Lab 6
Week 5 | 16 June 2020<br>Activity 11, Lab 7 |18 June 2020<br>Activity 12, Lab 8<br>Exam 2 &rarr; | &rarr; Exam 2 &rarr;
Week 6 | 23 June 2020<br>&rarr; Exam 2<br>Activity 13, Lab 9 | 25 June 2020<br>Activity 14, Lab 10
Week 7 | 30 June 2020<br>Activity 15, Lab 11<br>Exam 3 &rarr; | 2 July 2020<br> &rarr; Exam 3 &rarr; | Friday, 3 July 2020<br>&rarr; Exam 3<br>(Due at 11:59pm)

## Copyright and License

#### &copy;2020 Karl R. Wurst, Worcester State University

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit
<a href="http://creativecommons.org/licenses/by-sa/4.0/" target="_blank">http://creativecommons.org/licenses/by-sa/4.0/</a> or send a
letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
